/********************************************************************************
 * The contents of this file are subject to the GNU General Public License      *
 * (GPL) Version 2 or later (the "License"); you may not use this file except   *
 * in compliance with the License. You may obtain a copy of the License at      *
 * http://www.gnu.org/copyleft/gpl.html                                         *
 *                                                                              *
 * Software distributed under the License is distributed on an "AS IS" basis,   *
 * without warranty of any kind, either expressed or implied. See the License   *
 * for the specific language governing rights and limitations under the         *
 * License.                                                                     *
 *                                                                              *
 * This file was originally developed as part of the software suite that        *
 * supports the book "The Elements of Computing Systems" by Nisan and Schocken, *
 * MIT Press 2005. If you modify the contents of this file, please document and *
 * mark your changes clearly, for the benefit of others.                        *
 ********************************************************************************/

package SimulatorsGUI;

import HackGUI.*;
import Hack.Utilities.Conversions;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;
import javax.swing.border.*;

/**
 * This class represents a 16-bits binary number.
 */
public class BinaryComponent extends JPanel implements MouseListener, KeyListener {

    // Creating the text fields.
    private JTextField bit0 = new JTextField(1);
    private JTextField bit1 = new JTextField(1);
    private JTextField bit2 = new JTextField(1);
    private JTextField bit3 = new JTextField(1);
    private JTextField bit4 = new JTextField(1);
    private JTextField bit5 = new JTextField(1);
    private JTextField bit6 = new JTextField(1);
    private JTextField bit7 = new JTextField(1);
    private JTextField bit8 = new JTextField(1);
    private JTextField bit9 = new JTextField(1);
    private JTextField bit10 = new JTextField(1);
    private JTextField bit11 = new JTextField(1);
    private JTextField bit12 = new JTextField(1);
    private JTextField bit13 = new JTextField(1);
    private JTextField bit14 = new JTextField(1);
    private JTextField bit15 = new JTextField(1);

    // An array containing all of the text fields.
    private JTextField[] bits = new JTextField[16];

    // The value of this component in a String representation.
    private StringBuffer valueStr;

    // Creating buttons.
    private JButton okButton = new JButton();
    private JButton cancelButton = new JButton();

    // Creating icons.
    private ImageIcon okIcon = new ImageIcon(Utilities.imagesDir + "smallok.gif");
    private ImageIcon cancelIcon = new ImageIcon(Utilities.imagesDir + "smallcancel.gif");

    // A vector conatining the listeners to this component.
    private Vector listeners;

    // A boolean value which is true if the user pressed the ok button and
    // false otherwise.
    private boolean isOk = false;

    // The border of this component.
    private Border binaryBorder;

    // The number of available bits.
    private int numberOfBits;

    /**
     * Constructs a new BinaryComponent.
     */
    public BinaryComponent() {
        super(new GridBagLayout());

        listeners = new Vector();

        jbInit();
    }

    /**
     * Registers the given PinValueListener as a listener to this component.
     */
    public void addListener (PinValueListener listener) {
        listeners.addElement(listener);
    }

    /**
     * Un-registers the given PinValueListener from being a listener to this component.
     */
    public void removeListener (PinValueListener listener) {
        listeners.removeElement(listener);
    }

    /**
     * Notify all the PinValueListeners on actions taken in it, by creating a
     * PinValueEvent and sending it using the pinValueChanged method to all
     * of the listeners.
     */
    public void notifyListeners () {
        PinValueEvent event = new PinValueEvent(this,valueStr.toString(),isOk);
        for(int i=0;i<listeners.size();i++) {
            ((PinValueListener)listeners.elementAt(i)).pinValueChanged(event);
        }
    }

    /**
     * Sets the number of bits of this component.
     */
    public void setNumOfBits (int num) {
        numberOfBits = num;
        for (int i=0; i<bits.length; i++) {
            if(i<bits.length - num) {
                bits[i].setText("");
                bits[i].setBackground(Color.darkGray);
                bits[i].setEnabled(false);
            }
            else {
                bits[i].setBackground(Color.white);
                bits[i].setEnabled(true);
            }
        }
    }

    /**
     * Sets the value of this component.
     */
    public void setValue (short value) {
        valueStr = new StringBuffer(Conversions.decimalToBinary(value,16));
        for (int i=0; i<bits.length; i++) {
            bits[i].setText(String.valueOf(valueStr.charAt(i)));
        }
    }

    /**
     * Returns the value of this component.
     */
    public short getValue() {
        return (short)Conversions.binaryToInt(valueStr.toString());
    }

    // Updates the value of this component.
    private void updateValue() {
        valueStr = new StringBuffer(16);
        char currentChar;
        for(int i=0; i<bits.length; i++) {
            if (bits[i].getText().equals(""))
                currentChar = '0';
            else
                currentChar = bits[i].getText().charAt(0);
            valueStr.append(currentChar);
        }
    }

    /**
     * Implementing the action of double-clicking the mouse on the text field.
     * "0" --> "1"
     * "1" --> "0"
     */
    public void mouseClicked(MouseEvent e) {
        if (e.getClickCount() == 2) {
            JTextField t = (JTextField)e.getSource();
            if(t.getText().equals("0"))
                t.setText("1");
            else if (t.getText().equals("1"))
                t.setText("0");
        }
    }

    /**
     * Implementing the action of inserting a letter into the text field,
	 * or pressing enter / escape.
     */
    public void keyTyped (KeyEvent e) {
        JTextField t = (JTextField)e.getSource();
        if(e.getKeyChar()=='0' || e.getKeyChar()=='1') {
            t.transferFocus();
            t.selectAll();
        } else if (e.getKeyChar() == Event.ENTER) {
			approve();
		} else if (e.getKeyChar() == Event.ESCAPE) {
			hideBinary();
		} else {
            t.selectAll();
            t.getToolkit().beep();
        }
    }

    // Empty implementations
    public void mouseExited (MouseEvent e) {}
    public void mouseEntered (MouseEvent e) {}
    public void mouseReleased (MouseEvent e) {}
    public void mousePressed (MouseEvent e) {}
    public void keyReleased (KeyEvent e) {}
    public void keyPressed (KeyEvent e) {}

    // Initialization of this component.
    private void jbInit() {
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.BOTH;
        c.ipadx = 3;
        c.insets = new Insets(5, 0, 5, 0);

        // Initial position, is decremented in the styling method
        c.gridx = 15;

        binaryBorder = BorderFactory.createLineBorder(Color.black,3);

        styleAndAddPreviousBit(bit0, c);
        styleAndAddPreviousBit(bit1, c);
        styleAndAddPreviousBit(bit2, c);
        styleAndAddPreviousBit(bit3, c);
        styleAndAddPreviousBit(bit4, c);
        styleAndAddPreviousBit(bit5, c);
        styleAndAddPreviousBit(bit6, c);
        styleAndAddPreviousBit(bit7, c);
        styleAndAddPreviousBit(bit8, c);
        styleAndAddPreviousBit(bit9, c);
        styleAndAddPreviousBit(bit10, c);
        styleAndAddPreviousBit(bit11, c);
        styleAndAddPreviousBit(bit12, c);
        styleAndAddPreviousBit(bit13, c);
        styleAndAddPreviousBit(bit14, c);
        styleAndAddPreviousBit(bit15, c);

        c.gridx = 4;
        c.gridy = 1;
        c.gridwidth = 4;
        okButton.setHorizontalTextPosition(SwingConstants.CENTER);
        okButton.setIcon(okIcon);
        okButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                okButton_actionPerformed(e);
            }
        });
        this.add(okButton, c);

        c.gridx = 8;
        cancelButton.setHorizontalTextPosition(SwingConstants.CENTER);
        cancelButton.setIcon(cancelIcon);
        cancelButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                cancelButton_actionPerformed(e);
            }
        });
        this.add(cancelButton, c);

        this.setBorder(binaryBorder);

        bits[0] = bit15; bits[1] = bit14; bits[2] = bit13; bits[3] = bit12;
        bits[4] = bit11; bits[5] = bit10; bits[6] = bit9; bits[7] = bit8;
        bits[8] = bit7; bits[9] = bit6; bits[10] = bit5; bits[11] = bit4;
        bits[12] = bit3; bits[13] = bit2; bits[14] = bit1; bits[15] = bit0;

        setMinimumSize(getMinimumSize());
    }

    private void styleAndAddPreviousBit(JTextField bit, GridBagConstraints c) {
        bit.setFont(Utilities.valueFont);
        bit.setText("0");
        bit.setHorizontalAlignment(SwingConstants.CENTER);
        bit.addMouseListener(this);
        bit.addKeyListener(this);
        this.add(bit, c);
        c.gridx -= 1;
    }

	/**
	 * Approve the change (called when OK is pressed or ENTER is pressed
	 */
	private void approve() {
        isOk = true;
        updateValue();
        notifyListeners();
        setVisible(false);
	}

    /**
     * Implementing the action of pressing the ok button.
     */
    public void okButton_actionPerformed(ActionEvent e) {
		approve();
    }

    /**
     * Implementing the action of pressing the cancel button.
     */
    public void cancelButton_actionPerformed(ActionEvent e) {
		hideBinary();
    }

    /**
     * Hides the binary component as though the cancel button was pressed
     */
    public void hideBinary() {
        isOk = false;
        notifyListeners();
        setVisible(false);
    }

    /**
     * Shows the Binary component and gives focus to the first available bit.
     */
    public void showBinary() {
        setVisible(true);
        bits[16 - numberOfBits].grabFocus();
    }
}
