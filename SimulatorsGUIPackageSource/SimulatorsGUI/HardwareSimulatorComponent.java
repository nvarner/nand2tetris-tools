/********************************************************************************
 * The contents of this file are subject to the GNU General Public License      *
 * (GPL) Version 2 or later (the "License"); you may not use this file except   *
 * in compliance with the License. You may obtain a copy of the License at      *
 * http://www.gnu.org/copyleft/gpl.html                                         *
 *                                                                              *
 * Software distributed under the License is distributed on an "AS IS" basis,   *
 * without warranty of any kind, either expressed or implied. See the License   *
 * for the specific language governing rights and limitations under the         *
 * License.                                                                     *
 *                                                                              *
 * This file was originally developed as part of the software suite that        *
 * supports the book "The Elements of Computing Systems" by Nisan and Schocken, *
 * MIT Press 2005. If you modify the contents of this file, please document and *
 * mark your changes clearly, for the benefit of others.                        *
 ********************************************************************************/

package SimulatorsGUI;

import Hack.Gates.*;
import Hack.ComputerParts.*;
import Hack.HardwareSimulator.*;
import HackGUI.*;
import javax.swing.*;
import java.awt.*;
import java.io.*;

/**
 * This class represents the gui of the hardware simulator.
 */
public class HardwareSimulatorComponent extends HackSimulatorComponent implements HardwareSimulatorGUI, GatesPanelGUI {

    // The input pins of the hardware simulator.
    private PinsComponent inputPins;

    // The output pins of the hardware simulator.
    private PinsComponent outputPins;

    // The internal pins of the hardware simulator.
    private PinsComponent internalPins;

    // The hdl view of the hardware simulator.
    private TextFileComponent hdlView;

    // The part pins of the hardware simulator.
    private PartPinsComponent partPins;

    // The parts of the hardware simulator.
    private PartsComponent parts;

    // The panel that always holds the additional display.
    private JPanel additionalDisplayPanel;

    // The optional gates panel
    private JPanel gatesPanel;

    // The panel that holds the screen and keyboard.
    private JPanel ioPanel;

    // The panel that holds the ROM and RAM.
    private JPanel memoryPanel;

    // The panels that hold only RAM and ROM.
    private JPanel ramPanel;
    private JPanel romPanel;

    // The panel that holds registers.
    private JPanel registerPanel;

    // The panel that holds the ALU and any unknown gates.
    private JPanel aluPanel;

    // The gate info component of the current gate
    private GateInfoComponent gateInfo;

    /**
     * Constructs a new HardwareSimulatorComponent.
     */
    public HardwareSimulatorComponent() {
        additionalDisplayPanel = new JPanel();
        gatesPanel = new JPanel();
        ioPanel = new JPanel();
        memoryPanel = new JPanel();
        ramPanel = new JPanel();
        romPanel = new JPanel();
        registerPanel = new JPanel();
        aluPanel = new JPanel();

        inputPins = new PinsComponent();
        outputPins = new PinsComponent();
        internalPins = new PinsComponent();
        partPins = new PartPinsComponent();
        parts = new PartsComponent();
        hdlView = new TextFileComponent();
        gateInfo = new GateInfoComponent();

        jbInit();
    }

    public void loadProgram() {}

    /**
     * Returns the Gates panel.
     */
    public GatesPanelGUI getGatesPanel() {
        return this;
    }

    /**
     * Returns the HDLView.
     */
    public TextFileGUI getHDLView() {
        return hdlView;
    }

    /**
     * Returns the input pins table.
     */
    public PinsGUI getInputPins() {
        return inputPins;
    }

    /**
     * Returns the output pins table.
     */
    public PinsGUI getOutputPins() {
        return outputPins;
    }

    /**
     * Returns the internal pins table.
     */
    public PinsGUI getInternalPins() {
        return internalPins;
    }

    public GateInfoGUI getGateInfo() {
        return gateInfo;
    }

    /**
     * Returns the part pins table.
     */
    public PartPinsGUI getPartPins() {
        return partPins;
    }

    /**
     * Returns the parts table.
     */
    public PartsGUI getParts() {
        return parts;
    }

    /**
     * Displays the Internal pins table.
     */
    public void showInternalPins() {
        internalPins.setVisible(true);
    }

    /**
     * Hides the Internal pins table.
     */
    public void hideInternalPins() {
        internalPins.setVisible(false);
    }

    /**
     * Displays the Part pins table.
     */
    public void showPartPins() {
        partPins.setVisible(true);
    }

    /**
     * Hides the Part pins table.
     */
    public void hidePartPins() {
        partPins.setVisible(false);
    }

    /**
     * Displays the Parts table.
     */
    public void showParts() {
        parts.setVisible(true);
    }

    /**
     * Hides the Parts table.
     */
    public void hideParts() {
        parts.setVisible(false);
    }

    public Container getAdditionalDisplayParent() {
        return additionalDisplayPanel;
    }

    public void setWorkingDir(File file) {
    }

    /**
     * Adds the given gate component to the gates panel.
     */
    public void addGateComponent(Component gateComponent) {
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.BOTH;
        c.weightx = 1;
        c.weighty = 1;

        if (gateComponent instanceof PointedMemoryComponent) {
            if (gateComponent instanceof ROMComponent) {
                romPanel.add(gateComponent, c);
            } else {
                ramPanel.add(gateComponent, c);
            }
        } else if (gateComponent instanceof RegisterComponent) {
            c.insets = new Insets(1, 6, 1, 6);
            c.weightx = 0;
            c.gridx = registerPanel.getComponentCount();
            registerPanel.add(gateComponent, c);
        } else {
            if (gateComponent instanceof ScreenComponent) {
                c.weighty = 0;
                c.fill = GridBagConstraints.NONE;

                ioPanel.add(gateComponent, c);
            } else if (gateComponent instanceof KeyboardComponent) {
                c.gridy = 1;
                c.weighty = 0;
                c.fill = GridBagConstraints.NONE;
                c.insets = new Insets(2, 0, 0, 0);

                ioPanel.add(gateComponent, c);
            } else {
                aluPanel.add(gateComponent, null);
            }
        }

        gatesPanel.revalidate();
        gatesPanel.repaint();
        gatesPanel.setVisible(true);
        registerPanel.setMinimumSize(registerPanel.getSize());
    }

    /**
     * Removes all the gate components from the gates panel.
     */
    public void removeAllGateComponents() {
        ioPanel.removeAll();
        ramPanel.removeAll();
        romPanel.removeAll();
        registerPanel.removeAll();
        aluPanel.removeAll();

        gatesPanel.revalidate();
        gatesPanel.repaint();

        if (currentAdditionalDisplay == null) {
            gatesPanel.setVisible(false);
        }
    }

    // Initialization of this component.
    private void jbInit() {
        this.setLayout(new GridBagLayout());
        additionalDisplayPanel.setLayout(new BorderLayout());
        gatesPanel.setLayout(new GridBagLayout());
        ioPanel.setLayout(new GridBagLayout());
        memoryPanel.setLayout(new BoxLayout(memoryPanel, BoxLayout.X_AXIS));
        ramPanel.setLayout(new GridBagLayout());
        romPanel.setLayout(new GridBagLayout());
        registerPanel.setLayout(new GridBagLayout());
        aluPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 1, 1));

        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.BOTH;
        c.insets = new Insets(2, 2, 2, 2);

        c.weightx = 1;

        c.gridy = 0;
        c.gridx = 0;
        c.gridwidth = 2;
        this.add(gateInfo, c);

        c.weighty = 1;

        c.insets = new Insets(2, 2, 0, 0);
        c.gridy = 2;
        c.gridwidth = 1;
        hdlView.setMinimumVisibleRows(15);
        hdlView.setName("HDL");
        this.add(hdlView, c);

        c.insets = new Insets(2, 2, 6, 0);
        c.gridy = 1;
        inputPins.setMinimumVisibleRows(15);
        inputPins.setPinsName("Input pins");
        inputPins.setTopLevelLocation(this);
        this.add(inputPins, c);

        c.insets = new Insets(2, 1, 6, 2);
        c.gridx = 1;
        outputPins.setMinimumVisibleRows(15);
        outputPins.setPinsName("Output pins");
        outputPins.setTopLevelLocation(this);
        this.add(outputPins, c);

        // Internal pins trades with part pins when a line in HDL is clicked that uses internal pins
        c.insets = new Insets(2, 1, 0, 2);
        c.gridy = 2;
        internalPins.setMinimumVisibleRows(15);
        internalPins.setVisible(false);
        internalPins.setPinsName("Internal pins");
        internalPins.setTopLevelLocation(this);
        this.add(internalPins, c);

        // Parts trades with part pins when the "PARTS:" line in HDL is clicked
        parts.setMinimumVisibleRows(15);
        parts.setVisible(false);
        parts.setName("Internal Parts");
        this.add(parts, c);

        partPins.setMinimumVisibleRows(15);
        partPins.setVisible(false);
        partPins.setPinsName("Part pins");
        partPins.setTopLevelLocation(this);
        this.add(partPins, c);

        c.gridx = 2;
        c.gridy = 0;
        c.gridheight = 3;
        c.weightx = 2;
        additionalDisplayPanel.setMinimumSize(new Dimension(524, 592));
        this.add(additionalDisplayPanel, c);

        // Set up gates panel with IO on top, then memory, registers, and ALU.
        c = new GridBagConstraints();
        c.fill = GridBagConstraints.BOTH;
        c.weightx = 1;
        c.insets = new Insets(0, 0, 4, 0);

        gatesPanel.add(ioPanel, c);

        memoryPanel.add(ramPanel, null);
        memoryPanel.add(Box.createRigidArea(new Dimension(130, 0)));
        memoryPanel.add(romPanel, null);

        c.weighty = 1;
        c.gridy = 1;
        gatesPanel.add(memoryPanel, c);

        c.insets = new Insets(0, 0, 0, 0);
        c.weighty = 0;
        c.gridy = 2;
        gatesPanel.add(registerPanel, c);

        c.insets = new Insets(0, 0, 0, 0);
        c.gridy = 3;
        gatesPanel.add(aluPanel, c);

        gatesPanel.setBorder(BorderFactory.createEtchedBorder());
        additionalDisplayPanel.add(gatesPanel, null);
    }

    public void setAdditionalDisplay(JComponent additionalComponent) {
        if (currentAdditionalDisplay == null && additionalComponent != null)
            gatesPanel.setVisible(false);
        else if (currentAdditionalDisplay != null && additionalComponent == null)
            gatesPanel.setVisible(true);

        super.setAdditionalDisplay(additionalComponent);
    }
}
