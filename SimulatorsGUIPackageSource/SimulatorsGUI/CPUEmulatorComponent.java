/********************************************************************************
 * The contents of this file are subject to the GNU General Public License      *
 * (GPL) Version 2 or later (the "License"); you may not use this file except   *
 * in compliance with the License. You may obtain a copy of the License at      *
 * http://www.gnu.org/copyleft/gpl.html                                         *
 *                                                                              *
 * Software distributed under the License is distributed on an "AS IS" basis,   *
 * without warranty of any kind, either expressed or implied. See the License   *
 * for the specific language governing rights and limitations under the         *
 * License.                                                                     *
 *                                                                              *
 * This file was originally developed as part of the software suite that        *
 * supports the book "The Elements of Computing Systems" by Nisan and Schocken, *
 * MIT Press 2005. If you modify the contents of this file, please document and *
 * mark your changes clearly, for the benefit of others.                        *
 ********************************************************************************/

package SimulatorsGUI;

import HackGUI.*;
import Hack.CPUEmulator.*;
import Hack.ComputerParts.*;

import javax.swing.*;
import java.awt.*;
import java.io.*;

/**
 * This class represents the gui of the CPUEmulator.
 */
public class CPUEmulatorComponent extends HackSimulatorComponent implements CPUEmulatorGUI  {

    // The container that holds everything but the bus.
    private JPanel mainContainer;

    // The container that holds the ROM, RAM, PC, and A register
    private JPanel leftContainer;

    // The container that initially holds the screen and later the script, output, and compare components.
    private JPanel additionalDisplayContainer;

    // Creating the RegisterComponents a, d and pc.
    private RegisterComponent a;
    private RegisterComponent d;
    private RegisterComponent pc;

    // The screen of the CPUEmulator.
    private ScreenComponent screen;

    // The keyboard of the CPUEmulator.
    private KeyboardComponent keyboard;

    // The memory of the CPUEmulator.
    private PointedMemoryComponent ram;

    // The ROM of the CPUEmulator.
    private ROMComponent rom;

    // The ALU of the CPUEmulator.
    private ALUComponent alu;

    // The bus of the CPUEmulator.
    private BusComponent bus;


    /**
     * Constructs a new CPUEmulatorComponent.
     */
    public CPUEmulatorComponent() {
        mainContainer = new JPanel();
        leftContainer = new JPanel();
        additionalDisplayContainer = new JPanel();

        screen = new ScreenComponent();
        keyboard = new KeyboardComponent();
        ram = new PointedMemoryComponent();
        rom = new ROMComponent();
        alu = new ALUComponent();
        a = new RegisterComponent();
        d = new RegisterComponent();
        pc = new RegisterComponent();
        bus = new BusComponent();

        jbInit();
    }

    public void setWorkingDir(File file) {
        rom.setWorkingDir(file);
    }

    public void loadProgram() {
        rom.loadProgram();
    }

    public Container getAdditionalDisplayParent() {
        return additionalDisplayContainer;
    }

    /**
     * Returns the alu GUI component.
     */
    public ALUGUI getALU() {
        return alu;
    }

    /**
     * Returns the bus GUI component.
     */
    public BusGUI getBus() {
        return bus;
    }

    /**
     * Returns the screen GUI component.
     */
    public ScreenGUI getScreen() {
        return screen;
    }

    /**
     * Returns the keyboard GUI component.
     */
    public KeyboardGUI getKeyboard() {
        return keyboard;
    }

    /**
     * Returns the RAM GUI component.
     */
    public PointedMemoryGUI getRAM() {
        return ram;
    }

    /**
     * Returns the ROM GUI component.
     */
    public ROMGUI getROM() {
        return rom;
    }

    /**
     * Returns the screen GUI component.
     */
    public RegisterGUI getA() {
        return a;
    }

    /**
     * Returns the screen GUI component.
     */
    public RegisterGUI getD() {
        return d;
    }

    /**
     * Returns the screen GUI component.
     */
    public RegisterGUI getPC() {
        return pc;
    }

    // Initialization of this component.
    private void jbInit() {
        this.setLayout(new OverlayLayout(this));
        mainContainer.setLayout(new GridBagLayout());
        leftContainer.setLayout(new GridBagLayout());
        additionalDisplayContainer.setLayout(new BorderLayout());

        this.add(bus, null);
        this.add(mainContainer, null);

        GridBagConstraints c = new GridBagConstraints();

        c.fill = GridBagConstraints.BOTH;
        c.weightx = 1;
        c.weighty = 1;
        c.gridx = 0;
        c.gridy = 0;
        c.gridheight = 5;
        mainContainer.add(leftContainer, c);

        // Left container: ROM, RAM, PC, A
        c.gridheight = 1;
        c.insets = new Insets(15, 15, 0, 35);
        rom.setMinimumVisibleRows(29);
        rom.setName("ROM");
        rom.setTopLevelLocation(this);
        leftContainer.add(rom, c);

        c.insets = new Insets(15, 15, 0, 0);
        c.gridx = 1;
        ram.setMinimumVisibleRows(29);
        ram.setName("RAM");
        ram.setTopLevelLocation(this);
        leftContainer.add(ram, c);

        c.fill = GridBagConstraints.NONE;
        c.weightx = 0;
        c.weighty = 0;
        c.insets = new Insets(10, 0, 10, 0);
        c.gridx = 0;
        c.gridy = 1;
        pc.setName("PC");
        leftContainer.add(pc, c);

        c.gridx = 1;
        a.setName("A");
        leftContainer.add(a, c);

        // Rest of main container (Screen, Keyboard, D, ALU)
        c.insets = new Insets(15, 20, 0, 20);
        c.weightx = 1;
        c.gridx = 2;
        c.gridy = 0;
        additionalDisplayContainer.setMinimumSize(screen.getMinimumSize());
        additionalDisplayContainer.setPreferredSize(screen.getMinimumSize());
        mainContainer.add(additionalDisplayContainer, c);

        screen.setToolTipText("Screen");
        additionalDisplayContainer.add(screen, null);

        c.anchor = GridBagConstraints.CENTER;
        c.weightx = 0;
        c.insets = new Insets(0, 20, 20, 20);
        c.gridy = 1;
        mainContainer.add(keyboard, c);

        c.insets = new Insets(15, 20, 20, 20);
        c.gridy = 2;
        d.setName("D");
        mainContainer.add(d, c);

        c.gridy = 3;
        mainContainer.add(alu, c);
    }

    public void setAdditionalDisplay(JComponent additionalComponent) {
        if (currentAdditionalDisplay == null && additionalComponent != null)
            screen.setVisible(false);
        else if (currentAdditionalDisplay != null && additionalComponent == null)
            screen.setVisible(true);

        super.setAdditionalDisplay(additionalComponent);
    }
}
