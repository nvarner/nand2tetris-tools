/********************************************************************************
 * The contents of this file are subject to the GNU General Public License      *
 * (GPL) Version 2 or later (the "License"); you may not use this file except   *
 * in compliance with the License. You may obtain a copy of the License at      *
 * http://www.gnu.org/copyleft/gpl.html                                         *
 *                                                                              *
 * Software distributed under the License is distributed on an "AS IS" basis,   *
 * without warranty of any kind, either expressed or implied. See the License   *
 * for the specific language governing rights and limitations under the         *
 * License.                                                                     *
 *                                                                              *
 * This file was originally developed as part of the software suite that        *
 * supports the book "The Elements of Computing Systems" by Nisan and Schocken, *
 * MIT Press 2005. If you modify the contents of this file, please document and *
 * mark your changes clearly, for the benefit of others.                        *
 ********************************************************************************/

package SimulatorsGUI;

import HackGUI.*;
import Hack.HardwareSimulator.*;
import javax.swing.*;
import java.awt.*;

/**
 * This class represents the GUI of a gate info.
 */
public class GateInfoComponent extends JPanel implements GateInfoGUI {

    // creating labels
    private JLabel chipNameLbl;
    private JLabel timeLbl;

    // creating text fields
    private JTextField chipNameTxt;
    private JTextField timeTxt;

    // true if the clock is currently up
    private boolean clockUp;

    // the name of the chip
    private String chipName;

    /**
     * Constructs a new GateInfoComponent.
     */
    public GateInfoComponent() {
        super(new GridBagLayout());

        chipNameLbl = new JLabel();
        timeLbl = new JLabel();

        chipNameTxt = new JTextField();
        timeTxt = new JTextField();

        jbInit();
    }

    public void setChip (String chipName) {
        this.chipName = chipName;
        chipNameTxt.setText(chipName);
    }

    public void setClock (boolean up) {
        clockUp = up;
        if(up)
            timeTxt.setText(timeTxt.getText() + "+");
    }

    public void setClocked (boolean clocked) {
        if(clocked)
            chipNameTxt.setText(chipName + " (Clocked) ");
        else
            chipNameTxt.setText(chipName);
    }


    public void setTime (int time) {
         if (clockUp)
            timeTxt.setText(String.valueOf(time) + "+");
        else
            timeTxt.setText(String.valueOf(time));
    }


    public void reset() {
        chipNameTxt.setText("");
        timeTxt.setText("0");
    }

    public void enableTime() {
        timeLbl.setEnabled(true);
        timeTxt.setEnabled(true);
    }

    public void disableTime() {
        timeLbl.setEnabled(false);
        timeTxt.setEnabled(false);
    }

    // Initializes this component.
    private void jbInit() {
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.BOTH;
        c.insets = new Insets(3, 7, 3, 7);

        chipNameLbl.setText("Chip Name :");
        chipNameLbl.setMinimumSize(new Dimension(74, 21));
        chipNameLbl.setBounds(new Rectangle(11, 7, 74, 21));
        this.add(chipNameLbl, c);

        c.gridx = 2;
        timeLbl.setMinimumSize(new Dimension(42, 21));
        timeLbl.setText("Time :");
        this.add(timeLbl, c);

        c.gridx = 1;
        c.weightx = 3;
        chipNameTxt.setBackground(SystemColor.info);
        chipNameTxt.setFont(Utilities.thinBigLabelsFont);
        chipNameTxt.setEditable(false);
        chipNameTxt.setHorizontalAlignment(SwingConstants.LEFT);
        chipNameTxt.setMinimumSize(new Dimension(231, 20));
        this.add(chipNameTxt, c);

        c.gridx = 3;
        c.weightx = 1;
        timeTxt.setBackground(SystemColor.info);
        timeTxt.setFont(Utilities.thinBigLabelsFont);
        timeTxt.setEditable(false);
        timeTxt.setMinimumSize(new Dimension(69, 20));
        this.add(timeTxt, c);

        setMinimumSize(new Dimension(483,37));
        setBorder(BorderFactory.createEtchedBorder());

        setMinimumSize(getMinimumSize());
    }
}
