/********************************************************************************
 * The contents of this file are subject to the GNU General Public License      *
 * (GPL) Version 2 or later (the "License"); you may not use this file except   *
 * in compliance with the License. You may obtain a copy of the License at      *
 * http://www.gnu.org/copyleft/gpl.html                                         *
 *                                                                              *
 * Software distributed under the License is distributed on an "AS IS" basis,   *
 * without warranty of any kind, either expressed or implied. See the License   *
 * for the specific language governing rights and limitations under the         *
 * License.                                                                     *
 *                                                                              *
 * This file was originally developed as part of the software suite that        *
 * supports the book "The Elements of Computing Systems" by Nisan and Schocken, *
 * MIT Press 2005. If you modify the contents of this file, please document and *
 * mark your changes clearly, for the benefit of others.                        *
 ********************************************************************************/

package SimulatorsGUI;

import HackGUI.*;
import Hack.VMEmulator.*;
import Hack.CPUEmulator.*;
import Hack.ComputerParts.*;
import java.awt.*;
import javax.swing.*;
import java.io.*;

/**
 * This class represents the gui of the VMEmulator.
 */
public class VMEmulatorComponent extends HackSimulatorComponent implements VMEmulatorGUI {

    // The container that holds everything but the bus.
    private JPanel mainContainer;

    // The container that holds the Program, Stack, and Call Stack.
    private JPanel leftContainer;

    // The container that holds the Global Stack and RAM.
    private JPanel bottomContainer;

    // The container that initially holds the screen and later the script, output, and compare components.
    private JPanel additionalDisplayContainer;

    // The keyboard of the VMEmulator component.
    private KeyboardComponent keyboard;

    // The screen of the VMEmulator component.
    private ScreenComponent screen;

    // The call stack of the VMEmulator component.
    private CallStackComponent callStack;

    // The program of the VMEmulator component.
    private ProgramComponent program;

    // The ram of the VMEmulator component.
    private LabeledMemoryComponent ram;

    // The stack of the VMEmulator component.
    private AbsolutePointedMemorySegmentComponent stack;

    // The memory segments of the VMEmulator component.
    private MemorySegmentsComponent segments;

    // The bus of the VMEmulator component.
    private BusComponent bus;

    // The calculator of this emulator.
    private StackCalculator calculator;

    // The working stack of the VMEmulator component.
    private TrimmedValuesOnlyAbsoluteMemorySegmentComponent workingStack;


    /**
     * Constructs a new VMEmulatorGUI.
     */
    public VMEmulatorComponent() {
        mainContainer = new JPanel();
        leftContainer = new JPanel();
        bottomContainer = new JPanel();
        additionalDisplayContainer = new JPanel();

        bus = new BusComponent();
        screen = new ScreenComponent();
        keyboard = new KeyboardComponent();
        ram = new LabeledMemoryComponent();
        callStack = new CallStackComponent();
        program = new ProgramComponent();
        segments = new MemorySegmentsComponent();
        workingStack = new TrimmedValuesOnlyAbsoluteMemorySegmentComponent();
        stack = new AbsolutePointedMemorySegmentComponent();
        calculator = new StackCalculator();

        jbInit();
    }

    public void setWorkingDir(File file) {
        program.setWorkingDir(file);
    }

    public void loadProgram() {
        program.loadProgram();
    }

    /**
     * Returns the calculator GUI component.
     */
    public CalculatorGUI getCalculator() {
        return calculator;
    }

    /**
     * Returns the bus GUI component.
     */
    public BusGUI getBus() {
        return bus;
    }

    /**
     * Returns the screen GUI component.
     */
    public ScreenGUI getScreen() {
        return screen;
    }

    /**
     * Returns the keyboard GUI component.
     */
    public KeyboardGUI getKeyboard() {
        return keyboard;
    }

    /**
     * Returns the RAM GUI component.
     */
    public LabeledPointedMemoryGUI getRAM() {
        return ram;
    }

    /**
     * Returns the Program GUI component.
     */
    public VMProgramGUI getProgram() {
        return program;
    }

    /**
     * Returns the call stack GUI component.
     */
    public CallStackGUI getCallStack() {
        return callStack;
    }

    /**
     * Returns the Stack GUI component.
     */
    public PointedMemorySegmentGUI getStack() {
        return stack;
    }

    /**
     * Returns the static memory segment component.
     */
    public MemorySegmentGUI getStaticSegment() {
        return segments.getStaticSegment();
    }

    /**
     * Returns the local memory segment component.
     */
    public MemorySegmentGUI getLocalSegment() {
        return segments.getLocalSegment();
    }

    /**
     * Returns the arg memory segment component.
     */
    public MemorySegmentGUI getArgSegment() {
        return segments.getArgSegment();
    }

    /**
     * Returns the this memory segment component.
     */
    public MemorySegmentGUI getThisSegment() {
        return segments.getThisSegment();
    }

    /**
     * Returns the that memory segment component.
     */
    public MemorySegmentGUI getThatSegment() {
        return segments.getThatSegment();
    }

    /**
     * Returns the temp memory segment component.
     */
    public MemorySegmentGUI getTempSegment() {
        return segments.getTempSegment();
    }

    /**
     * Returns the working stack.
     */
    public PointedMemorySegmentGUI getWorkingStack() {
        return workingStack;
    }

    public Container getAdditionalDisplayParent() {
        return additionalDisplayContainer;
    }

    // Sets the memory component of the memory segments with the current RAM.
    private void setSegmentsRam() {
        // Setting the memory of the segments.
        segments.getStaticSegment().setMemoryComponent(ram);
        segments.getLocalSegment().setMemoryComponent(ram);
        segments.getArgSegment().setMemoryComponent(ram);
        segments.getThisSegment().setMemoryComponent(ram);
        segments.getThatSegment().setMemoryComponent(ram);
        segments.getTempSegment().setMemoryComponent(ram);
        stack.setMemoryComponent(ram);
        workingStack.setMemoryComponent(ram);
        //registers the segments to listen to the repain event of the ram.
        ram.addChangeListener(segments.getStaticSegment());
        ram.addChangeListener(segments.getLocalSegment());
        ram.addChangeListener(segments.getArgSegment());
        ram.addChangeListener(segments.getThisSegment());
        ram.addChangeListener(segments.getThatSegment());
        ram.addChangeListener(segments.getTempSegment());
        ram.addChangeListener(stack);
        ram.addChangeListener(workingStack);
    }

    // Initialization of this component.
    private void jbInit() {
        setSegmentsRam();

        this.setLayout(new OverlayLayout(this));
        mainContainer.setLayout(new GridBagLayout());
        leftContainer.setLayout(new GridBagLayout());
        bottomContainer.setLayout(new GridBagLayout());
        additionalDisplayContainer.setLayout(new BorderLayout());

        this.add(bus, null);
        this.add(mainContainer, null);

        GridBagConstraints c = new GridBagConstraints();

        c.gridwidth = 2;
        c.weightx = 1;
        c.weighty = 2;
        c.fill = GridBagConstraints.BOTH;
        program.setMinimumVisibleRows(15);
        leftContainer.add(program, c);

        c.insets = new Insets(25, 0, 10, 0);
        c.gridwidth = 1;
        c.weighty = 1;
        c.gridy = 1;
        workingStack.setMinimumVisibleRows(7);
        workingStack.setSegmentName("Stack");
        workingStack.setTopLevelLocation(this);
        leftContainer.add(workingStack, c);

        c.insets = new Insets(0, 0, 0, 0);
        c.gridx = 1;
        calculator.setBorder(BorderFactory.createLoweredBevelBorder());
        calculator.setVisible(false);
        leftContainer.add(calculator, c);

        c.gridwidth = 2;
        c.gridx = 0;
        c.gridy = 2;
        callStack.setMinimumVisibleRows(7);
        leftContainer.add(callStack, c);

        c = new GridBagConstraints();

        c.weighty = 1;
        c.weightx = 1;
        c.fill = GridBagConstraints.BOTH;
        c.gridheight = 3;
        mainContainer.add(leftContainer, c);

        c.insets = new Insets(0, 60, 0, 10);
        c.gridx = 1;
        segments.getStaticSegment().setTopLevelLocation(this);
        segments.getLocalSegment().setTopLevelLocation(this);
        segments.getArgSegment().setTopLevelLocation(this);
        segments.getThisSegment().setTopLevelLocation(this);
        segments.getThatSegment().setTopLevelLocation(this);
        segments.getTempSegment().setTopLevelLocation(this);
        mainContainer.add(segments.getSplitPane(), c);

        c.fill = GridBagConstraints.NONE;
        c.anchor = GridBagConstraints.CENTER;
        c.insets = new Insets(0, 0, 0, 0);
        c.weighty = 0;
        c.gridheight = 1;
        c.gridwidth = 2;
        c.gridx = 2;
        additionalDisplayContainer.setMinimumSize(screen.getMinimumSize());
        mainContainer.add(additionalDisplayContainer, c);

        additionalDisplayContainer.add(screen, null);

        c.gridy = 1;
        mainContainer.add(keyboard, c);

        c.fill = GridBagConstraints.BOTH;
        c.insets = new Insets(20, 10, 0, 10);
        c.weighty = 1;
        c.gridwidth = 1;
        c.gridy = 2;
        stack.setMinimumVisibleRows(15);
        stack.setSegmentName("Global Stack");
        stack.setTopLevelLocation(this);
        mainContainer.add(stack, c);

        c.gridx = 3;
        ram.setMinimumVisibleRows(15);
        ram.setName("RAM");
        ram.setTopLevelLocation(this);
        mainContainer.add(ram, c);
    }

    public void setAdditionalDisplay(JComponent additionalComponent) {
        if (currentAdditionalDisplay == null && additionalComponent != null)
            screen.setVisible(false);
        else if (currentAdditionalDisplay != null && additionalComponent == null)
            screen.setVisible(true);

        super.setAdditionalDisplay(additionalComponent);
    }
}
