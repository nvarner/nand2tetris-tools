#!/bin/bash
shopt -s globstar
cd "$(dirname "$0")" || exit

release=7

# Compile a directory of .java files and zip the directory into a JAR
# Parameters:
#  1. directory to compile and JAR
#  2. what to save the JAR as (will be saved under ./InstallDir/bin/lib/<name>.jar)
#  3. immediate child directory of 1st parameter directory
#
# Preconditions:
#  - There's a folder called temp that this script can write to in the PWD
#  - InstallDir/bin/lib/ exists and the script can write to it
compile_and_jar() {
  javac "$1"/**/*.java -d temp -cp "InstallDir/bin/lib/*" --release $release
  cd temp || exit
  jar cvf "../InstallDir/bin/lib/$2.jar" "$3"
  cd ..
  rm -rf temp/*
}

# Compile most of the source into JAR files and put in bin/lib/
rm -f InstallDir/bin/lib/*
mkdir -p InstallDir/bin/lib/
mkdir -p temp/
compile_and_jar HackPackageSource Hack Hack
compile_and_jar HackGUIPackageSource HackGUI HackGUI
compile_and_jar CompilersPackageSource Compilers Hack
compile_and_jar SimulatorsPackageSource Simulators Hack
compile_and_jar SimulatorsGUIPackageSource SimulatorsGUI SimulatorsGUI
rm -r temp

# Compile Java implementations of HDL chips and put in builtInChips/
rm -f InstallDir/builtInChips/*.class
javac BuiltInChipsSource/*.java -d InstallDir -cp "InstallDir/bin/lib/*" --release $release

# Compile Java implementation of VM commands and put in builtInVMCode/
rm -f InstallDir/builtInVMCode/*.class
javac BuiltInVMCodeSource/*.java -d InstallDir -cp "InstallDir/bin/lib/*" --release $release

# Compile main classes and put in bin/classes/
rm -f InstallDir/bin/classes/*.class
javac MainClassesSource/*.java -d InstallDir/bin/classes -cp "InstallDir/bin/lib/*" --release $release

# Zip into output archive
rm -f nand2tetris.zip
cd InstallDir || exit
jar cvfM ../nand2tetris.zip .
