#!/usr/bin/env sh

rm -f nand2tetris_updated_source.zip
zip -r nand2tetris_updated_source.zip ./* -x .idea/\* -x out/\* -x .gitignore -x .gitlab-ci.yml -x Compile.sh -x CreateSourceZip.sh
